#!/usr/bin/env python3

import argparse
import logging
import account_close


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to close/purge account')
    parser.add_argument('action', help='action', choices=['purge', 'close'])
    parser.add_argument('user', help='your backoffice user with valid rights')
    parser.add_argument('account', help='account to close or purge', nargs='+')
    parser.add_argument('-b', '--broker-url', help='broker url. Default is broker://instant.retail.ghcg.com',
                        action='store', default='broker://instant.retail.ghcg.com')
    parser.add_argument('-c', '--counterparty', help='counterparty. Default is xCFD',
                        action='store', default='XCFD')
    parser.add_argument('-e', '--environment', help='environment. Default is test',
                        action='store', default='test',
                        choices=['prod', 'demo', 'retail', 'retail-demo', 'test', 'stage'])
    parser.add_argument('--dry-run', help='dry dun. Default is False',
                        action='store_true', default=False)
    parser.add_argument('--log', help='log file. Default is None',
                        action='store', default=None)
    parser.add_argument('--log-format', help='log formating', action='store',
                        default='%(asctime)s : %(levelname)s : %(funcName)s : %(message)s')
    parser.add_argument('--log-level', help='log level. Default is warning',
                        action='store', default='warning',
                        choices=['debug', 'info', 'warning', 'error', 'critical'])
    args = parser.parse_args()

    # apply logging settings
    loglevel = getattr(logging, args.log_level.upper())
    logging.basicConfig(filename=args.log, format=args.log_format, level=loglevel)

    # build symbol list
    ac = account_close.AccountClose(args.environment, args.user, args.counterparty, args.broker_url, 0.0)
    for acc in args.account:
        if args.action == 'purge':
            if args.dry_run:
                print('Going to purge {}'.format(acc))
            else:
                status = ac.purge(acc)
                print('(time: {time:%Y-%m-%dT%H:%M:%S}; account: {account}, \
orders: {orders}, {order_statuses}; \
positions: {positions}, {position_statuses})'.format(**status))
        elif args.action == 'close':
            if args.dry_run:
                print('Going to close {}'.format(acc))
            else:
                status = ac.close(acc)
                print('(time: {time:%Y-%m-%dT%H:%M:%S}; account: {account}, \
orders: {orders}, {order_statuses}; \
positions: {positions}, {position_statuses}'.format(**status))
                if status.get('next'):
                    print('Next time to place order {next:%Y-%m-%dT%H:%M:%S}'.format(**status))
