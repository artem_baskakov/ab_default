#!/usr/bin/env python

import logging
import time
import requests
from monitor_client import Incident, Indicator


class MonitorEventWorker(object):
    '''
    class which sends messages to monitor
    '''

    __indicators = dict()
    __interval = None
    __logger = None
    __monitor_client = None
    __monitor_name = None

    def __init__(self, monitor_name, monitor_client, interval):
        '''
        class init method
        :param monitor_name: monitor name
        :param monitor_client: pointer to the MonitorClient class
        :param interval: update interval
        :param url: monitor url
        '''
        self.__logger = logging.getLogger(monitor_name)
        self.__interval = interval
        self.__monitor_name = monitor_name
        self.__monitor_client = monitor_client

    @property
    def indicator(self, name):
        '''
        initializated indicator by name
        :param name: indicator name from add_indicator method
        :return: pointer to Indicator object or None
        '''
        return self.__indicators.get(name)

    @property
    def interval(self):
        '''
        check interval
        :return: integer interval
        '''
        return self.__interval

    @property
    def monitor_name(self):
        '''
        monitor name
        :return: current monitor name as string
        '''
        return self.__monitor_name

    @property
    def registered_paths(self):
        '''
        get all registerd paths
        :return: list of registered paths
        '''
        return [ind['path'] for ind in self.__indicators.values()]

    def add_indicator(self, name, path, run_method):
        '''
        init indicator
        :param name: new indicator name
        :param path: new indicator path
        :param run_method: run method to check new data
        :param data: indicator message
        '''
        self.__indicators[name] = {
            'object': Indicator(name),
            'name': name,
            'path': path,
            'run': run_method
        }
        self.__monitor_client.updateIndicator(path, self.__indicators[name]['object'])

    def run(self):
        '''
        run this worker
        '''
        while True:
            # sleep
            time.sleep(self.__interval)
            # update indicators
            indicators = self.__indicators.values()
            for ind in indicators:
                self.__logger.info('Update indicator {}'.format(ind['name']))
                if not ind['path']:
                    self.__logger.warning('Found empty path, skiping', exc_info=True)
                    continue
                data = ind['run'](ind['path'])
                ind['object'].setState(data['status'], data['statusName'],
                                       data['statusType'], data['description'])
                if data['status'] is None:
                    self.__indicators.pop(ind['name'], None)

    def send_incident(self, inc_type, subject, message, key=None):
        '''
        create new incident
        :param inc_type: incident type
        :param subject: incident subject
        :param message: incident message
        :param key: key if any
        '''
        self.__monitor_client.createIncident(Incident(inc_type, subject, message, key))

    def __repr__(self):
        '''
        representation method
        :return: string for this method
        '''
        return 'MonitorEventWorker({}, {}, [{}])'.format(self.__monitor_name,
                                                         self.__interval,
                                                         [ind['path'] for ind in self.__indicators.values()])
