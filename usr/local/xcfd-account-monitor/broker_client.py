#!/usr/bin/env python3

import datetime
import json
import logging
import os
import requests
import subprocess
import uuid
from dateutil import parser as date_parser


class BrokerClientResponse(object):
    '''
    additional class
    '''

    __code = ''
    __status = False
    __reason = ''

    def __init__(self, status, reason='', code=''):
        '''
        init class method
        :param status: status
        :param reason: reason
        '''
        self.__code = code
        self.__status = status
        self.__reason = reason

    @property
    def code(self):
        '''
        return broker client response code
        '''
        return self.__code

    @property
    def status(self):
        '''
        return broker client response status (true of false)
        '''
        return self.__status

    @property
    def reason(self):
        '''
        return broker client response message if any
        '''
        return self.__reason


class OrderRequest(object):
    '''
    additional class
    '''

    def __init__(self, **kwargs):
        '''
        init class method
        '''
        self.__data = dict()
        for name, value in kwargs.items():
            self.__data[name] = value

    def set_value(self, name, value):
        '''
        set value with given name
        :param name: value name
        :param value: value
        '''
        self.__data[name] = value

    def value(self, name):
        '''
        :param name: value name
        :return: value for given name
        '''
        return self.__data.get(name)

    def __str__(self):
        '''
        string representation
        '''
        return str(self.__data)


class BrokerClient:
    '''python wrapper for broker-client.jar'''

    cmd = 'java -jar {} --ui json'
    env = None
    path = None
    url = 'http://nexus.ghcg.com/service/local/repositories/ghcg-internal/content/com/ghcg/gateway/broker-client/16.1.0/broker-client-16.1.0-jar-with-dependencies.jar'

    def __init__(self, env='prod', path='broker-client.jar'):
        '''
        class init method
        :param env: environment
        :param path: path to jar executable
        '''
        self.env = env
        self.path = path

    def __build_proc(self):
        '''
        method to build process
        :return: pointer to process
        '''
        self.check_app()
        command = self.cmd.format(self.path)
        return subprocess.Popen(command.split(), bufsize=1, stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    def __connect(self, _id, gateway, stdin):
        '''
        method to send connect request
        :param _id: unique ID
        :param gateway; gateway url to connect
        :param stdin: stream to data be written
        '''
        payload = {
            'command': 'connect',
            'connection': _id,
            'url': gateway if gateway.startswith('broker://') else 'broker://{}'.format(gateway)
        }
        logging.info('broker-client payload is {}'.format(payload))
        return self.__put(payload, stdin)

    def __disconnect(self, _id, stdin):
        '''
        method to send disconnect request
        :param _id: ID to disconnect
        :param stdin: stream to data be written
        '''
        payload = {
            'command': 'disconnect',
            'connection': _id
        }
        logging.info('broker-client payload is {}'.format(payload))
        return self.__put(payload, stdin)

    def __do(self, orders, action, status, fields):
        '''
        do work
        :param orders: orders list
        :param action: action to do
        :param status: status which is True
        :param fields: copy this fields
        :return: modified order list
        '''
        self.version()
        proc = self.__build_proc()
        logging.info('Started PID {}'.format(proc.pid))
        # prepare
        ## assing gateway IDs
        gateways = dict(
            (gw, str(uuid.uuid1())) for gw in set([order.value('brokerUrl') for order in orders]) if gw is not None
        )
        ## check errors if any
        if not orders or not gateways:
            logging.info('Nothing to do here')
            self.__terminate(proc)
            return orders
        ## connect
        for gw in gateways:
            self.__connect(gateways[gw], gw, proc.stdin)
        ## wait for connection
        while proc.poll() is None:
            broker_output = self.__read_json(proc.stdout)
            logging.info(broker_output)
            # connection established, go put an order
            if broker_output['event'] == 'connection_state' \
                    and broker_output['state'] == 'online':
                connection = broker_output['connection']
                # put
                for i, order in enumerate(orders):
                    if gateways.get(orders[i].value('brokerUrl')) != connection:
                        continue
                    payload = {
                        'command': action,
                        'connection': gateways.get(order.value('brokerUrl')),
                        'id': order.value('id'),
                        'username': order.value('username')
                    }
                    for field in fields:
                        if order.value(field) is None:
                            continue
                        payload[field] = order.value(field)
                    if any([v is None for v in payload.values()]):
                        logging.warning('Missing data found in {}'.format(order.__str__()))
                        orders[i].set_value('status', BrokerClientResponse(False, 'Missing data'))
                        continue
                    self.__put(payload, proc.stdin)
            # handle order updates
            elif broker_output['event'] == 'order_update':
                for i, order in enumerate(orders):
                    if order.value('id') != broker_output['id']:
                        continue
                    if broker_output['status'] == 'rejected' \
                            or broker_output['status'] == status:
                        orders[i].set_value('status', BrokerClientResponse(
                            broker_output['status'] == status,
                            broker_output.get('reason', dict()).get('internalMessage', ''),
                            broker_output.get('reason', dict()).get('code', ''),
                        ))
                    else:
                        logging.warning('Received status {status}, skipping'.format(**broker_output))
                    break
            # handle rejects
            elif broker_output['event'] == 'modification_reject':
                for i, order in enumerate(orders):
                    if order.value('id') != broker_output['id']:
                        continue
                    orders[i].set_value('status', BrokerClientResponse(
                        False,
                        broker_output.get('reason', dict()).get('internalMessage', ''),
                        broker_output.get('reason', dict()).get('code', '')
                    ))
                    break
            if all(order.value('status') is not None for order in orders):
                break
        # disconnect and exit
        for gw in gateways:
            self.__disconnect(gateways[gw], proc.stdin)
        self.__terminate(proc)
        return orders

    def __put(self, payload, stdin):
        '''
        puts a message
        :param payload: what to put
        :param stdin: stream to data be written
        '''
        stdin.write('{}\n'.format(json.dumps(payload)).encode('utf8'))
        stdin.flush()

    def __read_json(self, stdout):
        '''
        method to read json
        :param stdout: stream from data be written
        :return: parsed json
        '''
        json_output = json.loads(stdout.readline().decode('utf8'))
        stdout.flush()
        return json_output

    def __terminate(self, proc):
        '''
        method to terminate the process
        :param proc: pointer to the process
        '''
        proc.stdin.close()
        proc.stdout.close()
        proc.stderr.close()
        logging.info('Terminate PID {}'.format(proc.pid))
        proc.terminate()

    def check_app(self):
        '''
        check if app exists
        :return: is application exist
        '''
        if os.path.isfile(self.path):
            return True
        r = requests.get(self.url, stream=True, timeout=2)
        logging.debug(r.url)
        print('Downloading file {} from {}'.format(self.path, self.url))
        if not r.ok:
            return False
        with open(self.path, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)
                    f.flush()
            return self.check_app()
        return False

    def version(self):
        '''
        print and return adaptor version
        '''
        ver = '0.0.1'
        logging.info('Broker client verson is {}'.format(ver))
        return ver

    # main methods
    def cancel(self, orders):
        '''
        cancels specified orders
        :param orders: list of OrderRequest objects
        :return: modified order list
        '''
        for i, order in enumerate(orders):
            orders[i].set_value('newModificationId', str(uuid.uuid1()))
        return self.__do(orders, 'cancel', 'cancelled', [
            'currentModificationId',
            'newModificationId'
        ])

    def place(self, orders, wait_for='working'):
        '''
        place orders
        :param orders: list of OrderRequest objects
        :param wait_for: wait for this statuses
        :return: modified order list
        '''
        for i, order in enumerate(orders):
            orders[i].set_value('id', str(uuid.uuid1()))
        return self.__do(orders, 'place', wait_for, [
            'exanteAccount',
            'brokerAccount',
            'orderParameters',
            'scheduleBehavior'
        ])

    def replace(self, orders):
        '''
        replace orders
        :param orders: list of OrderRequest objects
        :return: modified order list
        '''
        for i, order in enumerate(orders):
            orders[i].set_value('newModificationId', str(uuid.uuid1()))
            orders[i].set_value('mode', 'auto')
        return self.__do(orders, 'replace', 'working', [
            'currentModificationId',
            'newModificationId',
            'quantity',
            'mode',
            'limitPrice',
            'stopPrice'
        ])
