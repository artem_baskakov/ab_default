#!/usr/bin/env python

import copy
import datetime
import logging
import requests
import threading
import time
from dateutil.parser import parse as date_parse
from dateutil.relativedelta import relativedelta
from requests.adapters import HTTPAdapter


class BackofficeLosersWorker(threading.Thread):
    '''
    filter account by given criteria
    '''

    __accounts = set()
    __as_worker = None
    __headers = {'content-type': 'application/json'}
    __interval = None
    __lct = datetime.datetime.utcnow()
    __lock = None
    __locked = dict()
    __logger = None
    __newbie = None
    __session = None
    __should_run = True
    __skip = set()
    __skip_users = list()
    __url = None
    __users_worker = None
    # properties
    __usd = None

    def __init__(self, usd, env, interval, skip_users, newbie, as_worker, users_worker):
        '''
        class init method
        :param usd: usd limit to be loser
        :param env: environment
        :param interval: update interval
        :param skip_users: users to skip
        :param newbie: imagine that loser is n00b if it was created less than this days
        :param as_worker: pointer to AS worker
        :param users_worker: pointer to account stream worker
        '''
        # thread properties
        threading.Thread.__init__(self)
        self.__lock = threading.Lock()
        self.daemon = True
        # main
        self.__logger = logging.getLogger('xcfd-account-monitor')
        self.__interval = interval
        self.__newbie = newbie
        self.__skip_users = skip_users
        self.__as_worker = as_worker
        self.__users_worker = users_worker
        # values
        self.__usd = usd
        self.__url = 'http://backoffice.{}.ghcg.com/api/v1.5/accounts/%s'.format(env)
        # we are using sessions instead of common libraries to increase performance
        self.__session = requests.Session()
        self.__session.mount('http://backoffice.{}.ghcg.com'.format(env), HTTPAdapter())

    @property
    def last_check_time(self):
        '''
        last check time
        :return: property string
        '''
        with self.__lock:
            return self.__lct

    @property
    def losers(self):
        '''
        get accounts data
        :return: list of accounts to close
        '''
        with self.__lock:
            accs = copy.deepcopy(self.__accounts)
            for acc in accs:
                self.__locked[acc] = True
            return accs

    def __get_account_creation_date(self, account):
        '''
        get account creation date
        :param account: account ID
        '''
        if not account:
            return datetime.datetime.utcnow()
        try:
            response = self.__session.get(self.__url % account, headers=self.__headers, timeout=30)
        except requests.exceptions.Timeout:
            self.__logger.warning('Timeout reached', exc_info=True)
            return datetime.datetime.utcnow()
        self.__logger.debug(response.url)
        try:
            return date_parse(response.json()['created'])
        except ValueError:
            self.__logger.error('Could not convert data to json', exc_info=True)
            return datetime.datetime.utcnow()
        except KeyError:
            self.__logger.error('No created key found', exc_info=True)
            return datetime.datetime.utcnow()

    def __is_newbie(self, created):
        '''
        check if account is newbie
        :param created: account creation date
        '''
        return created + relativedelta(days=self.__newbie) > datetime.datetime.utcnow()

    def clear_transfered(self, account):
        '''
        clear transfered accounts
        :param account: account to ignore
        '''
        with self.__lock:
            self.__locked[account] = False

    def drop_by_skip_list(self, account):
        '''
        one more ignore list management
        :param account: account to ignore
        '''
        with self.__lock:
            self.__skip.add(account)

    def run(self):
        '''
        run request
        '''
        while self.__should_run:
            try:
                with self.__lock:
                    all_accs = self.__as_worker.accounts
                    excluded_accs = copy.deepcopy(self.__skip_users)
                    for user in self.__skip_users:
                        excluded_accs.extend(self.__users_worker.accounts.get(user, list()))
                    # append source list
                    self.__accounts = set()
                    for acc in all_accs:
                        if self.__locked.get(acc, False):
                            self.__logger.warning('Account {} is already transferred by loser list, skipping'.format(acc))
                            continue
                        if all_accs[acc].get('USD', 0.0) < self.__usd \
                                and acc not in self.__skip \
                                and acc not in excluded_accs \
                                and not self.__is_newbie(self.__get_account_creation_date(acc)):
                            self.__logger.warning('Append {} to loser list with data {}'.format(acc, all_accs[acc]))
                            self.__accounts.add(acc)
                    self.__lct = datetime.datetime.utcnow()
            except Exception:
                self.__logger.error('Unexpected error', exc_info=True)
            time.sleep(self.__interval)

    def stop(self):
        '''
        stop monitor
        '''
        with self.__lock:
            self.__should_run = False
