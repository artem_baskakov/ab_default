#!/usr/bin/env python

import argparse
import logging
import xcfd_account_monitor_worker


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Account closer')
    parser.add_argument('-c', '--config', help='path to configuration file',
                        action='store', default=None)
    parser.add_argument('--log', help='log file. Default is None',
                        action='store', default=None)
    parser.add_argument('--log-format', help='log formating', action='store',
                        default='%(asctime)s : %(levelname)s : %(funcName)s : %(message)s')
    parser.add_argument('--log-level', help='log level. Default is warning',
                        action='store', default='warning',
                        choices=['debug', 'info', 'warning', 'error', 'critical'])
    args = parser.parse_args()

    # apply logging settings
    loglevel = getattr(logging, args.log_level.upper())
    logging.basicConfig(filename=args.log, format=args.log_format, level=loglevel)

    # configuration file
    cfg = 'xcfd-account-monitor.ini' if args.config is None else args.config

    # init
    worker = xcfd_account_monitor_worker.XCFDAccountMonitorWorker(cfg)
    worker.run()
