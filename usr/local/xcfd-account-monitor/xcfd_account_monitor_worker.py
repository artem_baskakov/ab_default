#!/usr/bin/env python

try:
    import ConfigParser
except ImportError:
    # fucking pytnon2->3 workaround, thank you Guido for our happy childhood
    import configparser as ConfigParser
import datetime
import logging
import time
from dateutil.parser import parse as date_parse
# core
import account_close
import backoffice_as_worker
import backoffice_losers_worker
import backoffice_mu_worker
import backoffice_perms_worker
import backoffice_worker
# libs part
import monitor_client
from monitor_event_worker import MonitorEventWorker


class XCFDAccountMonitorWorker(object):
    '''
    class to init orderdb monitor
    '''

    __monitor_worker = None
    __monitor_client = None
    __ac = None
    __autopurge_enabled = False
    __boasw = None
    __bolw = None
    __bomuw = None
    __bopw = None
    __bow = None
    __invert_skip = False
    __last = {
        'close/losers': list(),
        'close/margin-call': list(),
        'close/negative': list()
    }
    __limit = None
    __purge = None
    __purge_losers = None
    __skip = list()
    # notifications
    keep_alive = None
    keep_errors = None
    check_error_level = None
    check_warn_level = None
    stream_error_level = None
    stream_warn_level = None

    def __init__(self, cfg):
        '''
        class init method
        :param cfg: path to configuration
        '''
        config = ConfigParser.SafeConfigParser()
        config.read(cfg)
        env = config.get('monitor', 'environment')
        interval = config.getint('monitor', 'interval')

        self.__logger = logging.getLogger('xcfd-account-monitor')

        # register indicators
        name = 'account-monitor-{}'.format(env)
        self.__monitor_client = monitor_client.fromConfig(name, interval,
                                                          properties={
                                                              'project': 'techsupport',
                                                              'team': 'techsupport'
                                                          })
        self.__monitor_worker = MonitorEventWorker(name, self.__monitor_client,
                                                   interval)
        self.__monitor_worker.add_indicator('{}-asstream'.format(name), 'streams/account-summary', self.stream_status)
        self.__monitor_worker.add_indicator('{}-bostream'.format(name), 'streams/metrics', self.stream_status)
        self.__monitor_worker.add_indicator('{}-boaccs'.format(name), 'streams/permissions', self.stream_status)
        self.__monitor_worker.add_indicator('{}-bocheck'.format(name), 'checks/metrics', self.check_status)
        self.__monitor_worker.add_indicator('{}-losecheck'.format(name), 'checks/losers', self.check_status)
        self.__monitor_worker.add_indicator('{}-close'.format(name), 'close/margin-call', self.do_magic)
        self.__monitor_worker.add_indicator('{}-fuckers'.format(name), 'close/negative', self.do_magic)
        self.__monitor_worker.add_indicator('{}-losers'.format(name), 'close/losers', self.do_magic)

        # read global settings
        if config.has_option('settings', 'invert_skip'):
            self.__invert_skip = config.getboolean('settings', 'invert_skip')
        self.__autopurge_enabled = config.getboolean('settings', 'autopurge')
        self.__limit = config.getint('settings', 'limit')
        self.__purge = config.getboolean('settings', 'purge')
        self.__purge_losers = config.getboolean('settings', 'purge_losers')
        self.__skip = [filt for key, filt in config.items('skip')] if config.has_section('skip') else list()
        self.__logger.info('Using skip list {}'.format(self.__skip))
        # notifications
        self.keep_alive = config.getint('notify', 'keep_alive')
        self.keep_errors = config.getint('notify', 'keep_alive_errors')
        self.check_error_level = config.getint('notify', 'check_error_level')
        self.check_warn_level = config.getint('notify', 'check_warn_level')
        self.stream_error_level = config.getint('notify', 'stream_error_level')
        self.stream_warn_level = config.getint('notify', 'stream_warn_level')

        # read local settings
        # night times
        night_close = dict()
        if config.has_section('force_night_close'):
            night_close['enabled'] = config.getboolean('force_night_close', 'force_close')
            if night_close['enabled']:
                night_close['start'] = date_parse(config.get('force_night_close', 'start')).time()
                night_close['stop'] = date_parse(config.get('force_night_close', 'stop')).time()
        else:
            night_close['enabled'] = False
        # force close
        force_close_users = [
            user for key, user in config.items('force_close_users')
        ] if config.has_section('force_close_users') else list()
        # skip accounts for losers
        skip_loser_users = [
            user for key, user in config.items('losers_skip_users')
        ] if config.has_section('losers_skip_users') else list()
        # vip users to apply vip_negative_nav parameter
        vip_purge_users = [
            user for key, user in config.items('vip_purge_users')
        ] if config.has_section('vip_purge_users') else list()
        # site ask url
        site_ask_url = config.get('settings', 'site_ask_url') if config.has_option('settings', 'site_ask_url') else ''

        # init workers
        self.__ac = account_close.AccountClose(env, config.get('settings', 'user'),
                                               config.get('settings', 'counterparty'),
                                               config.get('settings', 'broker_url'),
                                               config.getfloat('settings', 'mu'),
                                               site_ask_url)
        self.__boasw = backoffice_as_worker.BackofficeASWorker(env)
        self.__bomuw = backoffice_mu_worker.BackofficeMUWorker(env)
        self.__bopw = backoffice_perms_worker.BackofficePermsWorker(env)
        self.__bolw = backoffice_losers_worker.BackofficeLosersWorker(config.getfloat('settings', 'losers_usd'),
                                                                      env, interval, skip_loser_users,
                                                                      config.getint('settings', 'purge_losers_older_than_days'),
                                                                      self.__boasw, self.__bopw)
        self.__bow = backoffice_worker.BackofficeWorker(config.getfloat('settings', 'mu'),
                                                        config.getfloat('settings', 'nav'),
                                                        config.getfloat('settings', 'vip_negative_nav'),
                                                        env, interval, night_close,
                                                        force_close_users, vip_purge_users,
                                                        self.__bomuw, self.__bopw)

    def __to_str(self, status):
        '''
        convert status to string
        '''
        return '(time: {time:%Y-%m-%dT%H:%M:%S}; account: {account}, \
orders: {orders}, {order_statuses}; \
positions: {positions}, {position_statuses})'.format(**status)

    def stream_status(self, path):
        '''
        common method for all streams status
        :param path: indicator path
        '''
        # define worker
        if path == 'streams/account-summary':
            worker = self.__boasw
        elif path == 'streams/metrics':
            worker = self.__bomuw
        elif path == 'streams/permissions':
            worker = self.__bopw
        else:
            raise Exception("Invalid path {}".format(path))
        # get status
        timediff = (datetime.datetime.utcnow() - worker.last_sync_time).total_seconds()
        # return status
        if not worker.status:
            return {
                'status': 'ERROR',
                'statusName': 'Stream broken',
                'statusType': 'failed',
                'description': 'Stream data broken, see logs for details (last \
message was {:.2f}s ago)'.format(timediff)
            }
        elif not worker.is_ready:
            return {
                'status': 'WARN',
                'statusName': 'Initializing',
                'statusType': 'initializing',
                'description': 'Stream is not ready yet'
            }
        elif self.stream_error_level != 0 and timediff > self.stream_error_level:
            return {
                'status': 'ERROR',
                'statusName': 'Data significantly delayed',
                'statusType': 'failed',
                'description': 'Last stream message was {:.2f}s ago'.format(timediff)
            }
        elif self.stream_warn_level != 0 and timediff > self.stream_warn_level:
            return {
                'status': 'WARN',
                'statusName': 'Data delayed',
                'statusType': 'failed',
                'description': 'Last stream message was {:.2f}s ago'.format(timediff)
            }
        else:
            return {
                'status': 'OK',
                'statusName': 'State OK',
                'statusType': 'working',
                'description': 'Backoffice stream is running (last message was \
{:.2f}s ago), loaded {} items'.format(timediff, worker.count)
            }

    def check_status(self, path):
        '''
        common method for all checks status
        :param pathL indicator path
        '''
        # define worker
        if path == 'checks/losers':
            worker = self.__bolw
        elif path == 'checks/metrics':
            worker = self.__bow
        else:
            raise Exception("Invalid path {}".format(path))
        # get status
        timediff = (datetime.datetime.utcnow() - worker.last_check_time).total_seconds()
        # return status
        if self.check_error_level != 0 and timediff > self.check_error_level:
            return {
                'status': 'ERROR',
                'statusName': 'Data significantly delayed',
                'statusType': 'failed',
                'description': 'Backoffice data delayed, last check was {:.2f}s ago'.format(timediff)
            }
        elif self.check_warn_level != 0 and timediff > self.check_warn_level:
            return {
                'status': 'WARN',
                'statusName': 'Data delayed',
                'statusType': 'failed',
                'description': 'Backoffice data significantly delayed, last check was {:.2f}s ago'.format(timediff)
            }
        else:
            return {
                'status': 'OK',
                'statusName': 'State OK',
                'statusType': 'working',
                'description': 'Backoffice checking is running'
            }

    def do_magic(self, path):
        '''
        close and purge em all
        :param path: indicator path
        '''
        # define worker
        if path == 'close/losers':
            worker = self.__bolw
            actual_copy = worker.losers
            allowed_to_do = self.__purge_losers
            close_method = self.__ac.purge
        elif path == 'close/margin-call':
            worker = self.__bow
            actual_copy = worker.accounts
            allowed_to_do = True
            close_method = self.__ac.close
        elif path == 'close/negative':
            worker = self.__bow
            actual_copy = worker.fuckers
            allowed_to_do = self.__purge
            close_method = self.__ac.purge
        else:
            raise Exception("Invalid path {}".format(path))
        # get list
        if self.__invert_skip:
            fuckers = [fucker for fucker in actual_copy if fucker in self.__skip and allowed_to_do]
        else:
            fuckers = [fucker for fucker in actual_copy if fucker not in self.__skip and allowed_to_do]
        # ask site what it thinks about it
        fuckers = self.__ac.ask_site(fuckers)
        # unlock skipped accounts
        for fucker in actual_copy:
            if fucker in fuckers:
                continue
            worker.drop_by_skip_list(fucker)
            worker.clear_transfered(fucker)
        # check limits
        if len(fuckers) > self.__limit:
            for fucker in fuckers:
                worker.clear_transfered(fucker)
            message = 'Latest accounts:\n\n{}'.format('\n'.join([
                self.__to_str(stat) for stat in self.__last[path]
            ]))
            return {
                'status': 'ERROR',
                'statusName': 'Limit reached',
                'statusType': 'failed',
                'description': 'Limit {} reached, found {} fuckers:\n{}\n\n{}'.format(
                    self.__limit, len(fuckers), ', '.join(fuckers), message)
            }
        # try to close em all
        status = list()
        for fucker in fuckers:
            st = close_method(fucker, self.__autopurge_enabled)
            if st.get('next'):
                self.__logger.warning('Next check for {} will be {}'.format(fucker, st['next']))
                self.__bomuw.set_timestamp(fucker, st['next'])
            status.append(st)
            worker.clear_transfered(fucker)
        # append list
        self.__last[path] = sorted(status, key=lambda k: k['time'], reverse=True) + self.__last[path]
        self.__last[path] = self.__last[path][:self.keep_alive]
        message = 'Latest accounts:\n\n{}'.format('\n'.join([
            self.__to_str(stat) for stat in self.__last[path]
        ]))
        # check statuses
        errors = [
            self.__to_str(stat) for stat in self.__last[path]
            if (datetime.datetime.utcnow() - stat['time']).total_seconds() < self.keep_errors and not stat['positions']
        ]
        warnings = [
            self.__to_str(stat) for stat in self.__last[path]
            if (datetime.datetime.utcnow() - stat['time']).total_seconds() < self.keep_errors and not stat['orders']
        ]
        if errors:
            return {
                'status': 'ERROR',
                'statusName': 'Error while do magic',
                'statusType': 'failed',
                'description': 'Could not process:\n{}\n\n{}'.format(
                    '\n'.join(errors), message)
            }
        elif warnings:
            return {
                'status': 'WARN',
                'statusName': 'Error while do magic',
                'statusType': 'failed',
                'description': 'Could not process:\n{}\n\n{}'.format(
                    '\n'.join(warnings), message)
            }
        else:
            return {
                'status': 'OK',
                'statusName': 'Accounts processed',
                'statusType': 'working',
                'description': message
            }

    def run(self):
        '''
        run worker
        '''
        # load permissions list first
        self.__bopw.start()
        while not self.__bopw.is_ready:
            self.__logger.warning('Loading permissions, please wait')
            time.sleep(10)
        self.__boasw.start()
        self.__bolw.start()
        self.__bomuw.start()
        self.__bow.start()
        self.__monitor_client.start()
        try:
            return self.__monitor_worker.run()
        except KeyboardInterrupt:
            self.__logger.warning('Keyboard interruption, exit', exc_info=True)
            exit()
