#!/usr/bin/env python

import copy
import datetime
import logging
import requests
import threading
import time
from requests.adapters import HTTPAdapter


class BackofficeWorker(threading.Thread):
    '''
    filter account by given criteria
    '''

    __accounts = set()
    __fuckers = set()
    __headers = {'content-type': 'application/json'}
    __interval = None
    __lct = datetime.datetime.utcnow()
    __lock = None
    __logger = None
    __mu_worker = None
    __night_close = None
    __session = None
    __should_run = True
    __skip = set()
    __locked = dict()
    __url = None
    __users_worker = None
    # properties
    __force_close_users = list()
    __mu = None
    __nav = None
    __vip_negative_nav = None
    __vip_purge_users = list()

    def __init__(self, mu, nav, vip_negative_nav, env, interval, night_close, force_close_users, vip_purge_users, mu_worker, users_worker):
        '''
        class init method
        :param mu: margin utilization limit
        :param nav: NAV USD limit
        :param vip_negative_nav: negative nav for vip clients to purge
        :param env: environment
        :param interval: update interval
        :param night_close: night close params
        :param force_close_users: force close accounts belong to these users
        :param vip_purge_users: user list to apply vip negative nav parameter
        :param mu_worker: pointer to MU worker
        :param users_worker: pointer to account stream worker
        '''
        # thread properties
        threading.Thread.__init__(self)
        self.__lock = threading.Lock()
        self.daemon = True
        # main
        self.__logger = logging.getLogger('xcfd-account-monitor')
        self.__interval = interval
        self.__mu_worker = mu_worker
        self.__users_worker = users_worker
        # values
        self.__force_close_users = force_close_users
        self.__mu = mu
        self.__nav = nav
        self.__vip_negative_nav = vip_negative_nav
        self.__vip_purge_users = vip_purge_users
        if self.__vip_negative_nav > 0.0:
            self.__logger.warning('VIP negative NAV {} more than 0.0, some functions may be unstable'.format(self.__vip_negative_nav))
        self.__night_close = night_close
        self.__url = 'http://backoffice.{}.ghcg.com/api/v1.5/transactions'.format(env)
        # we are using sessions instead of common libraries to increase performance
        self.__session = requests.Session()
        self.__session.mount('http://backoffice.{}.ghcg.com'.format(env), HTTPAdapter())

    @property
    def accounts(self):
        '''
        get accounts data
        :return: list of accounts to close
        '''
        with self.__lock:
            accs = copy.deepcopy(self.__accounts)
            for acc in accs:
                self.__locked[acc] = True
            return accs

    @property
    def fuckers(self):
        '''
        get accounts with nav < 0
        :return: list of accounts
        '''
        with self.__lock:
            accs = copy.deepcopy(self.__fuckers)
            for acc in accs:
                self.__locked[acc] = True
            return accs

    @property
    def last_check_time(self):
        '''
        last check time
        :return: property string
        '''
        with self.__lock:
            return self.__lct

    def __get_data_for_account(self, account):
        '''
        get transactions data for specified account
        :param account: account ID
        '''
        if not account:
            return list()
        try:
            response = self.__session.get(self.__url, headers=self.__headers,
                                          params={
                                              'accountId': account,
                                              'type': 'FUNDING/WITHDRAWAL'
                                          }, timeout=30)
        except requests.exceptions.Timeout:
            self.__logger.warning('Timeout reached', exc_info=True)
            return
        self.__logger.debug(response.url)
        try:
            return [
                tr for tr in response.json() if tr['asset'] != 'XSD'
            ]
        except ValueError:
            self.__logger.error('Could not convert data to json', exc_info=True)
            return

    def __get_data(self, accounts):
        '''
        get bo data for accounts
        :param accounts: accounts list
        '''
        transactions = list()
        while len(accounts) > 0:
            portion = self.__get_data_for_account('|'.join(acc for acc in accounts[:100]))
            if portion is None:
                raise RuntimeError('Could not get transactions')
            transactions.extend(portion)
            accounts = accounts[100:]
        return transactions

    def __is_night(self):
        '''
        check if now is night
        '''
        now = datetime.datetime.utcnow().time()
        return (self.__night_close['start'] < self.__night_close['stop']
                and (now > self.__night_close['start']
                    and now < self.__night_close['stop'])) \
            or (self.__night_close['start'] > self.__night_close['stop']
                and (now > self.__night_close['start']
                    or now < self.__night_close['stop']))

    def clear_transfered(self, account):
        '''
        clear transfered accounts
        :param account: account to ignore
        '''
        with self.__lock:
            self.__locked[account] = False

    def drop_by_skip_list(self, account):
        '''
        one more ignore list management
        :param account: account to ignore
        '''
        with self.__lock:
            self.__skip.add(account)

    def run(self):
        '''
        run request
        '''
        while self.__should_run:
            try:
                with self.__lock:
                    all_accs = self.__mu_worker.accounts
                    # first exception list
                    allowed_accs = copy.deepcopy(self.__force_close_users)
                    for user in self.__force_close_users:
                        allowed_accs.extend(self.__users_worker.accounts.get(user, list()))
                    # second exception list
                    vip_purge_accs = copy.deepcopy(self.__vip_purge_users)
                    for user in self.__vip_purge_users:
                        vip_purge_accs.extend(self.__users_worker.accounts.get(user, list()))
                    # build initial fucker list
                    accs = dict((acc, all_accs[acc]) for acc in all_accs
                                if acc not in self.__skip and
                                all_accs[acc]['mu'] > self.__mu and
                                all_accs[acc].get('ts', datetime.datetime(1970, 1, 1, 0, 0, 0)) < datetime.datetime.utcnow())
                    transactions = self.__get_data(accs.keys())
                    # append source list
                    self.__accounts = set()
                    self.__fuckers = set()
                    for acc in accs:
                        if self.__locked.get(acc, False):
                            self.__logger.warning('Account {} is already transferred by negative NAV or margin call, skipping'.format(acc))
                            continue
                        trans = [t for t in transactions if t['accountId'] == acc]
                        # add to list
                        if (accs[acc]['nav'] - accs[acc]['xsd']) < self.__vip_negative_nav and acc in vip_purge_accs:
                            self.__logger.warning('Append {} to vip fuckers list with data {}'.format(acc, accs[acc]))
                            self.__fuckers.add(acc)
                        elif accs[acc]['nav'] < 0.0 and acc not in vip_purge_accs:
                            self.__logger.warning('Append {} to non-vip fuckers list with data {}'.format(acc, accs[acc]))
                            self.__fuckers.add(acc)
                        elif accs[acc]['nav'] < self.__nav or not trans:
                            self.__logger.warning('Append {} to margin call list with data {}'.format(acc, accs[acc]))
                            self.__accounts.add(acc)
                        elif self.__night_close['enabled'] and self.__is_night():
                            self.__logger.warning('Append {} to margin call list because of night with data {}'.format(acc, accs[acc]))
                            self.__accounts.add(acc)
                        elif acc in allowed_accs:
                            self.__logger.warning('Append {} to margin call list because of allow list with data {}'.format(acc, accs[acc]))
                            self.__accounts.add(acc)
                    del transactions
                    self.__lct = datetime.datetime.utcnow()
            except Exception:
                self.__logger.error('Unexpected error', exc_info=True)
            time.sleep(self.__interval)

    def stop(self):
        '''
        stop monitor
        '''
        with self.__lock:
            self.__should_run = False
