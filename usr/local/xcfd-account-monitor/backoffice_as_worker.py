#!/usr/bin/env python

import copy
import datetime
import json
import logging
import requests
import threading
import time
from socket import error as SocketError


class BackofficeASWorker(threading.Thread):
    '''
    worker to stream account summary data from backoffice
    '''

    __accounts = dict()
    __headers = {'accept-encoding': 'gzip', 'accept': 'application/x-json-stream'}
    __lst = datetime.datetime.utcnow()
    __lock = None
    __logger = None
    __should_run = True
    __status = None
    __stream_url = None
    __ready = False

    def __init__(self, env):
        '''
        class init method
        :param env: environment
        '''
        # thread properties
        threading.Thread.__init__(self)
        self.__lock = threading.Lock()
        self.daemon = True
        # main
        self.__logger = logging.getLogger('xcfd-account-monitor')
        _url = 'http://backoffice.{}.ghcg.com'.format(env)
        self.__stream_url = '{}/api/v2.0/streams/positions'.format(_url)

    @property
    def accounts(self):
        '''
        get accounts data
        :return: dictionary for accounts
        '''
        with self.__lock:
            return copy.deepcopy(self.__accounts)

    @property
    def count(self):
        '''
        get account count
        :return: int
        '''
        with self.__lock:
            return len(self.__accounts)

    @property
    def is_ready(self):
        '''
        :return: is stream ready
        '''
        return self.__ready

    @property
    def last_sync_time(self):
        '''
        last check time
        :return: property string
        '''
        with self.__lock:
            return self.__lst

    @property
    def status(self):
        '''
        current stream status
        :return: bool
        '''
        return self.__status

    def __add_position(self, account, what, value):
        '''
        add position to account
        :param account: account ID
        :param what: symbol ID for position
        :param value: position value
        '''
        with self.__lock:
            if account in self.__accounts:
                self.__accounts[account][what] = float(value)
            else:
                self.__accounts[account] = {
                    what: float(value)
                }

    def __get_stream(self):
        '''
        get mu stream
        :return: iterator object
        '''
        response = requests.get(self.__stream_url,  stream=True, timeout=60,
                                headers=self.__headers)
        return response.iter_lines()

    def run(self):
        '''
        run request
        '''
        # we are using the cycle to avoid TRE
        while self.__should_run:
            try:
                for item in self.__get_stream():
                    self.__status = True
                    # exit on no actions
                    if not self.__should_run:
                        break
                    # work block
                    data = json.loads(item.decode('utf8'))
                    self.__logger.debug('Received data {}'.format(data))
                    # data['value'] != '-0.01' - see slack conversation
                    if data['$type'] == 'fx_position' and data['value'] != '-0.01':
                        self.__add_position(data['accountId'], data['currency'], data['value'])
                    elif data['$type'] == 'position' and data['value'] != '-0.01':
                        self.__add_position(data['accountId'], data['symbolId'], data['value'])
                    elif data['$type'] == 'heartbeat':
                        self.__logger.debug('Heartbeat received')
                    elif data['$type'] == 'sync':
                        self.__ready = True
                    with self.__lock:
                        self.__lst = datetime.datetime.utcnow()
            except requests.exceptions.Timeout:
                self.__logger.warning('Timeout reached', exc_info=True)
                self.__status = False
            except requests.exceptions.ChunkedEncodingError:
                self.__logger.warning('Chunk read failed', exc_info=True)
                self.__status = False
            except requests.ConnectionError:
                self.__logger.warning('Connection error', exc_info=True)
                self.__status = False
            except SocketError:
                self.__logger.warning('Socket error', exc_info=True)
                self.__status = False
            time.sleep(60)

    def stop(self):
        '''
        stop monitor
        '''
        with self.__lock:
            self.__should_run = False
