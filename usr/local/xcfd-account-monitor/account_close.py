#!/usr/bin/env python

import datetime
import logging
import requests
from socket import error as SocketError
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.exceptions import InsecurePlatformWarning
from requests.packages.urllib3.exceptions import InsecureRequestWarning
# internal
import broker_client

requests.packages.urllib3.disable_warnings(InsecurePlatformWarning)
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class AccountClose(object):
    '''
    simple worker to get fresh data about rejects
    '''

    __bc = None
    __bo_as_url = None
    __bo_close_url = None
    __bo_session = None
    __broker_url = None
    __counterparty = None
    __headers = {'content-type': 'application/json'}
    __hook_filters = {'symbols', 'symbolId', 'symbol', 'schedule', 'intervals', 'start'}
    __mu = None
    __positions = ('stock', 'option', 'fx_spot', 'future', 'fund', 'forex', 'cfd', 'bond')
    __logger = None
    __odb_url = None
    __odb_session = None
    __sdb_url = None
    __sdb_session = None
    __site_url = None
    __site_session = None
    __user = None

    def __init__(self, env, user, cp, bu, mu, ask_site_url=''):
        '''
        class init method
        :param env: environment
        :param user: user to close positions
        :param cp: counterparty
        :param bu: broker URL
        :param mu: margin utilization limit
        :param ask_site_url:
        '''
        # main
        self.__logger = logging.getLogger('xcfd-account-monitor')
        # values
        self.__bo_as_url = 'http://backoffice.{}.ghcg.com/api/v1.5/accounts/%s/summary/USD'.format(env)
        self.__bo_close_url = 'http://backoffice.{}.ghcg.com/api/v1.5/accounts/%s/close_all_positions'.format(env)
        self.__odb_url = 'http://orderdb.{}.ghcg.com/orders'.format(env)
        self.__sdb_url = 'http://symboldb.{}.ghcg.com/symboldb/api/v1/symbols'.format(env)
        self.__site_url = ask_site_url
        self.__broker_url = bu
        self.__counterparty = cp
        self.__mu = mu
        self.__user = user
        # we are using sessions instead of common libraries to increase performance
        self.__bo_session = requests.Session()
        self.__bo_session.mount('http://backoffice.{}.ghcg.com'.format(env), HTTPAdapter())
        self.__odb_session = requests.Session()
        self.__odb_session.mount('http://orderdb.{}.ghcg.com'.format(env), HTTPAdapter())
        self.__sdb_session = requests.Session()
        self.__sdb_session.mount('http://symboldb.{}.ghcg.com'.format(env), HTTPAdapter())
        if self.__site_url:
            self.__site_session = requests.Session()
            self.__site_session.mount(self.__site_url, HTTPAdapter())
        self.__bc = broker_client.BrokerClient(env)

    def __close_orders(self, account):
        '''
        cancel all working orders
        :param account: account ID
        :return: purge status
        '''
        orders = list()
        try:
            response = self.__odb_session.get(self.__odb_url, params={
                'account': account,
                'status': 'active'
            }, timeout=30)
        except requests.exceptions.Timeout:
            self.__logger.warning('Timeout reached', exc_info=True)
            return list()
        self.__logger.debug(response.url)
        try:
            orders_list = response.json().get('orders', list())
            for order in orders_list:
                payload = broker_client.OrderRequest(**order)
                payload.set_value('username', self.__user)
                orders.append(payload)
        except ValueError:
            self.__logger.warning('Could not get json data', exc_info=True)
        return self.__bc.cancel(orders)

    def __close_all_positions(self, account):
        '''
        close all positions for specified account
        :param account: account ID
        :return: request status
        '''
        try:
            response = self.__bo_session.post(self.__bo_close_url % account, timeout=30)
        except requests.exceptions.Timeout:
            self.__logger.warning('Timeout reached', exc_info=True)
            return False
        self.__logger.debug(response.url)
        return response.ok

    def __close_positions(self, positions, account):
        '''
        put an market orders in other direction
        :param positions: account positions list
        :param account: account ID
        :return: close status
        '''
        orders = list()
        if positions is None:
            return list()
        for pos in positions:
            side = 'sell' if -1 * float(pos['quantity']) < 0.0 else 'buy'
            payload = broker_client.OrderRequest()
            payload.set_value('username', self.__user)
            payload.set_value('exanteAccount', account)
            payload.set_value('brokerUrl', self.__broker_url)
            payload.set_value('brokerAccount', {
                'name': self.__counterparty,
                'clientId': None
            })
            payload.set_value('orderParameters', {
                'instrument': pos['symbolId'],
                'side': side,
                'quantity': abs(float(pos['quantity'])),
                'type': 'market',
                'duration': 'day'
            })
            orders.append(payload)
        return self.__bc.place(orders)

    def __sdb_schedule(self, instrument):
        '''
        get next schedule interval
        :param instrument: instrument
        :return: datetime object
        '''
        try:
            response = self.__sdb_session.get(self.__sdb_url, headers=self.__headers,
                                              params={
                                                  'symbol_id': instrument
                                              }, timeout=3)
        except requests.exceptions.Timeout:
            return
        self.__logger.debug(response.url)
        try:
            intervals = response.json(object_pairs_hook=lambda pairs:
                                      dict((k, v) for k, v in pairs
                                           if k in self.__hook_filters)).get('symbols', list())[0]['symbol']['schedule']['intervals']
        except IndexError:
            self.__logger.error('Could not find element', exc_info=True)
            return
        except ValueError:
            self.__logger.error('Could not convert data to json', exc_info=True)
            return
        except KeyError:
            self.__logger.error('No required key', exc_info=True)
            return
        interval = None
        for i in intervals:
            if datetime.datetime.fromtimestamp(i['start']) < datetime.datetime.utcnow():
                continue
            interval = datetime.datetime.fromtimestamp(i['start'])
            break
        return interval

    def __get_positions(self, account):
        '''
        get currency on position
        :param account: account ID
        :return: list of symbols on position
        '''
        try:
            response = self.__bo_session.get(self.__bo_as_url % account,
                                             timeout=30, headers=self.__headers)
        except requests.exceptions.Timeout:
            self.__logger.warning('Timeout reached', exc_info=True)
            return None, False
        self.__logger.debug(response.url)
        try:
            data = response.json()
            output = list()
            for pos in self.__positions:
                output.extend([position for position in data[pos]
                               if float(position['value']) != 0.0])
            return output, float(data['marginUtilization']) >= self.__mu
        except KeyError:
            self.__logger.warning('Could not get keys', exc_info=True)
            return None, False
        except ValueError:
            self.__logger.warning('Could not convert to json', exc_info=True)
            return None, False

    def ask_site(self, accounts):
        '''
        ask site are accounts allowed for purge
        :param accounts: accounts to ask
        :return: list of allowed accounts
        '''
        allowed = list()
        if self.__site_url:
            try:
                response = self.__site_session.post(self.__site_url, json={
                    'accounts': accounts
                }, verify=False, timeout=30)
            except requests.exceptions.Timeout:
                self.__logger.warning('Timeout reached', exc_info=True)
                return allowed
            except requests.ConnectionError:
                self.__logger.warning('Connection error', exc_info=True)
                return allowed
            except SocketError:
                self.__logger.warning('Socket error', exc_info=True)
                return allowed
            self.__logger.debug(response.url)
            try:
                allowed = response.json().get('allowed', list())
            except ValueError:
                self.__logger.warning('Could not get json data', exc_info=True)
        else:
            allowed = accounts
        if set(accounts) - set(allowed):
            self.__logger.warning('Not allowed by site: {}'.format(set(accounts) - set(allowed)))
        return allowed

    def close(self, account, autopurge=False):
        '''
        close specified account
        :param account: account ID
        :param autopurge: purge account automatically if no openned positions
        :return: status
        '''
        response = dict()
        response['time'] = datetime.datetime.utcnow()
        self.__logger.info('Going to close {}'.format(account))
        # go
        ord_res_status = self.__close_orders(account)
        response['account'] = account
        # orders
        response['orders'] = not ord_res_status or \
            all([order.value('status').status for order in ord_res_status])
        response['order_statuses'] = [
            (order.value('orderParameters').get('instrument'), order.value('status').code) for order in ord_res_status
        ]
        # positions
        positions, is_margin_call = self.__get_positions(account)
        # one more check to avoid possible race conditions
        if not is_margin_call:
            self.__logger.error('Account {} has no margin call'.format(account))
            response['positions'] = True
            response['position_statuses'] = list()
            return response
        # in cases were MU>1.1 and no open positions - only purge here
        if not positions and autopurge:
            self.__logger.warning('Force purge for {} activated'.format(account))
            return self.purge(account)
        # default behaviour
        ord_res_status = self.__close_positions(positions, account)
        response['positions'] = not ord_res_status or \
            all([order.value('status').status for order in ord_res_status])
        response['position_statuses'] = [
            (order.value('orderParameters').get('instrument'), order.value('status').code) for order in ord_res_status
        ]
        # append schedules
        if not response['positions']:
            times = list()
            for ind, order in enumerate(ord_res_status):
                if not order.value('status').status \
                        and order.value('status').code == 'market_closed':
                    times.append(self.__sdb_schedule(positions[ind]['symbolId']))
                    # override status here to avoid additional indicator
                    response['positions'] = True
            response['next'] = min(times) if times else None
        return response

    def purge(self, account, autopurge=True):
        '''
        purge specified account
        :param account: account ID
        :param autopurge: purge account automatically if no openned positions
        :return: status
        '''
        response = dict()
        self.__logger.info('Going to purge {}'.format(account))
        # go
        ord_res_status = self.__close_orders(account)
        response['account'] = account
        # orders
        response['orders'] = not ord_res_status or \
            any([order.value('status').status for order in ord_res_status])
        response['order_statuses'] = [
            (order.value('orderParameters').get('instrument'), order.value('status').code) for order in ord_res_status
        ]
        response['positions'] = self.__close_all_positions(account)
        response['position_statuses'] = list()
        response['time'] = datetime.datetime.utcnow()
        return response
