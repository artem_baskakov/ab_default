xcfd-account-monitor
====================

Monitor which used for checking accounts with margin call and close them. Also
may purge accounts with negative NAV.

Installation
------------

```
BUILD_VERSION=VER make
sudo dpkg -i blablabla
```

Requirements
------------

See `Makefile` for details.

Configuration
-------------

Default configuration path is `xcfd-account-monitor.ini`. The following fields are
required:

* `[monitor]` group:
    * `environment` - use OrderDB from this environment. Also this option affects
    monitor name.
    * `interval` - interval for updates. Keep in mind that monitor will mark module
    as offline if there were no updates in 30 secs.
* `[notify]` group:
    * `keep_alive` - keep alive this number of accounts.
    * `keep_alive_errors` - keep alive in second errors.
    * `check_error_level` - show `ERROR` indicator if there were no updates in
    this interval (secs) from transactions DB. `0` skips checking.
    * `check_warn_level` - show `WARN` indicator if there were no updates in
    this interval (secs) from transactions DB. `0` skips checking.
    * `stream_error_level` - show `ERROR` indicator if there were no updates in
    this interval (secs) from backoffice stream. `0` skips checking.
    * `stream_warn_level` - show `WARN` indicator if there were no updates in
    this interval (secs) from backoffice stream. `0` skips checking.
* `[settings]` group:
    * `autopurge` - autopurge accoutns if there is margin call but no open
    positions, boolean
    * `broker_url` - broker URL
    * `counterparty` - trade counterparty
    * `limit` - do not do anything if number of accounts with margin call more
    than this value, integer.
    * `losers_usd` - imagine that user is loser if its USD balance less than this
    value, float.
    * `mu` - close if margin untilization more than this value, double, e.g.
    `1.11`.
    * `nav` - do not close if NAV more than this value, double, e.g. `100.0`.
    Value is in backoffice value (most probably in EUR).
    * `purge` - purge accounts with negative NAV (i.e. set their NAVs to 0),
    boolean.
    * `purge_losers` - purge accounts which lost money, boolean.
    * `purge_losers_older_than_days` - do purge losers if account older than this
    days count, integer.
    * `user` - user which is used to cancel working orders, string. This user
    should have rights to do it.
    * `vip_negative_nav` - use this value of negative NAV instead of 0 for accounts
    mentioned in `[vip_purge_users]`.

The following fields are optional:

* `[settings]` group:
    * `invert_skip` - invert skip list. Default is `false`.
    * `site_ask_url` - ask site by using this url if account allowed to
    close/purge. Default is ''.
* `[skip]` group - describes accounts which should be skipped. One account per key,
the key names have no values.
* `[force_night_close]` group:
    * `force_close` - force close accounts by margin call during night, boolean.
If this field if false or group is missing, accounts will be closed using general
rights during night.
    * `start` - night start, UTC, e.g. `21:00:00`.
    * `stop` - night stop, UTC, e.g. `06:00:00`.
* `[force_close_users]` group - describes accounts or users which accounts will
be closed by margin call ignoring general rules. One user per key, the key names
have no values.
* `[losers_skip_users]` group - describes accounts or users which accounts
should not be closed by losers rules. One user per key, the key names have no
values.
* `[vip_purge_users]` group - describes accoutns or users which accounts should
be purged by using `vip_negative_nav` parameter. One user per key, the key names
have no values.

See `xcfd-account-monitor.ini.example` for some options.

Indicators
----------

* `close/*`:
    * `OK`:`working`:`Accounts processed` - all's ok.
    * `WARN`:`failed`:`Error while do magic` - there are errors while trying to
    cancel working orders.
    * `ERROR`:`failed`:`Error while do magic` - there are errors while trying to
    purge accounts, you will need to do it manually.
    * `ERROR`:`failed`:`Limit reached` - limit `settings.limit` reached. You
    should check it and purge manually.
* `checks/*`:
    * `OK`:`working`:`State OK` - all's ok.
    * `WARN`:`failed`:`Data delayed` - the last backoffice successfully check
    was more than `notify.check_warn_level` seconds ago. No actions required.
    * `ERROR`:`failed`:`Data significantly delayed` - the last backoffice
    successfully check was more than `notify.check_error_level` seconds ago.
    Check logs and restart if required.
* `streams/*`:
    * `OK`:`working`:`State OK` - all's ok.
    * `WARN`:`failed`:`Data delayed` - the last backoffice successfully check
    was more than `notify.stream_warn_level` seconds ago. No actions required.
    * `WARN`:`initializing`:`Initializing` - stream in sync.
    * `ERROR`:`failed`:`Data significantly delayed` - the last backoffice
    successfully check was more than `notify.stream_error_level` seconds ago.
    Check logs and restart if required.
    * `ERROR`:`failed`:`Stream broken` - an error occurs with backoffice stream,
    check logs and restart if required.

Logging
-------

Default log level is warning. If you want to decrease or increase log level use
`--log-level` command options. See `xcfd-account-monitor.py -h` for more logging
options.

By default logs are saved as `/var/log/supervisor/xcfd-account-monitor.log` (if
you are using `supervisor`).

Service
-------

The module is run under `supervisor`. To control it use `sudo supervisorctl COMMAND xcfd-account-monitor`,
e.g.:

```
sudo supervisorctl restart xcfd-account-monitor
```

Manual usage
------------

Script `manual_close.py` allows you to close or purge manually. See
`manual_close.py -h` for usage and details.
