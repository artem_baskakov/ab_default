all: deb

deb: clean
	fpm -s dir -t deb -n xcfd-account-monitor -v "0.0.$(BUILD_NUMBER)" \
	-d python \
	-d supervisor \
	-d python-monitor_client \
	-a all -C . \
	--deb-user 0 \
	--deb-group 0 \
	--prefix / \
	usr etc

clean:
	rm -rf *.deb
